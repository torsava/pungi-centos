from multilib import *
from stream-include-excludes import *
#from stream-images import *

# PRODUCT INFO
release_name = "CentOS Stream"
release_short = "CentOS-Stream"
release_version = "8"
version = "8"

# GENERAL SETTINGS
comps_file = {
    'scm': 'git',
    'repo': 'https://git.centos.org/centos/comps.git',
    'branch': 'master',
    'file': 'comps-centos-8-stream.xml',
}

link_type = 'hardlink-or-copy'
variants_file = 'variants-stream.xml'
sigkeys = ['8483C65D']
tree_arches = ['aarch64', 'i386', 'ppc64le', 'x86_64']

module_defaults_dir = {
    'scm': 'git',
    'repo': 'https://git.centos.org/centos/centos-module-defaults.git',
    'branch': 'c8s',
    'dir': '.',
}

multilib = [
    ('^.*$', {
        'x86_64': ['devel', 'runtime']
    }),
]

# RUNROOT settings
runroot_channel = 'image'
runroot_tag = 'dist-c8-build'
runroot_method = 'koji'

# PKGSET
pkgset_source = "koji"
koji_profile = "mbox"
pkgset_allow_reuse = False

# PKGSET - KOJI
pkgset_koji_tag = "dist-c8-stream-compose"
pkgset_koji_inherit = False
pkgset_koji_module_tag = "dist-c8-stream-module-compose"

filter_system_release_packages = False

# GATHER
gather_backend = "dnf"
check_deps = False
greedy_method = "none"
repoclosure_backend = "dnf"
gather_prepopulate = "centos-stream-packages.json"

gather_method = {
    "^(?!(AppStream|PowerTools|CR)).*$": {
        "comps": "deps",
    },
    "^(AppStream|PowerTools|CR)$": "hybrid",
}

hashed_directories = False

# CREATEREPO
createrepo_deltas = False
createrepo_database = True
createrepo_c = True
createrepo_checksum = "sha256"
createrepo_use_xz = True
createrepo_num_threads = 8
createrepo_num_workers = 8
createrepo_extra_args = ['--distro=cpe:/o:centos-stream:centos-stream:8,CentOS Stream 8', '--revision=8-stream']

# CHECKSUMS
media_checksums = ['sha256']
media_checksum_one_file = True
create_jigdo = False

# Pull in modulemd for older module versions to fill in gaps in contexts
createrepo_extra_modulemd = {
    "AppStream": {
        "scm": "git",
        "repo": "https://git.centos.org/centos/centos-metadata.git",
        "dir": "extra-modulemd",
    }
}

# BUILDINSTALL
bootable = True
buildinstall_method = "lorax"

lorax_options = [
    ("^.*$", {
        "*": {
            "noupgrade": False,
            "rootfs_size": 3,
            "version": "8-stream"
        }
    })
]

buildinstall_skip = [
    ("^(AppStream|PowerTools|CR|HighAvailability|RT|ResilientStorage|NFV)$", {
        "*": True
    }),
]

image_name_format = {
    "^BaseOS$": "{release_short}-{version}-{arch}-{date}-{disc_type}{disc_num}{suffix}",
    ".*": "{release_short}-{version}-{arch}-{date}-{disc_type}{disc_num}{suffix}",
}

image_volid_formats = [ '{release_short}-{version}-{arch}-{disc_type}', ]

# CREATEISO
create_optional_isos = False

createiso_skip = [
    ('^(BaseOS|AppStream|PowerTools|HighAvailability|CR|RT|ResilientStorage|NFV)$', {
        '*': True,
        'src':True
    }),
]

extra_isos = {
    "BaseOS": [{
        "include_variants": ["AppStream"],
        "filename": "{release_short}-{version}-{arch}-{date}-{disc_type}{disc_num}{suffix}",
        "skip_src": True,
    }]
}

restricted_volid = True

# LOOKASIDE
variant_as_lookaside = [
    ('AppStream', 'BaseOS'),
    ('PowerTools', 'BaseOS'),
    ('PowerTools', 'AppStream'),
    ('HighAvailability', 'BaseOS'),
    ('HighAvailability', 'AppStream'),
    ("ResilientStorage", "BaseOS"),
    ("ResilientStorage", "AppStream"),
    ("RT", "BaseOS"),
    ("RT", "AppStream"),
    ("NFV", "BaseOS"),
    ("NFV", "AppStream"),
]

extra_files = [
    ("^.*$", {
        "*": [
            {
                "scm": "git",
                "repo": "https://git.centos.org/rpms/centos-release.git",
                "branch": "c8",
                "file": [
                    'SOURCES/GPL',
                    'SOURCES/EULA',
                ],
                "target": "",
            },
        ],
    }),
]

productimg = False

translate_paths = [
        ("/mnt/koji", "https://koji.mbox.centos.org/pkgs"),
        ("/compose", "http://172.22.0.123/compose"),
]
